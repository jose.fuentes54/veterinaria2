import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('INICIO'),
        ),
        body: ListView(
          children: <Widget>[
            _opcionesMenu(context, Icons.person, 'formulario', 'Formulario')
          ],
        ));
  }
}

Widget _opcionesMenu(
    BuildContext context, IconData icono, String ruta, String nombre) {
  return ListTile(
    onTap: () => {Navigator.pushNamed(context, ruta)},
    leading: Icon(icono),
    title: Text(nombre),
    trailing: Icon(Icons.keyboard_arrow_right),
  );
}
