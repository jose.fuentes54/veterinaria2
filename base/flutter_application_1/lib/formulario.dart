import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum sexo { hombre, mujer }

class Formulario extends StatefulWidget {
  const Formulario({super.key});

  @override
  State<Formulario> createState() => _Formulario();
}

class _Formulario extends State<Formulario> {
  TextEditingController _date = TextEditingController();
  sexo? _character = sexo.hombre;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FORMULARIO PRUEBA '),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: _rut()),
              Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: _correo()),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: _nombre()),
              Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: _apellido()),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          _direccion(),
          const SizedBox(
            height: 20,
          ),
          ListTile(
            title: const Text('Mujer'),
            leading: Radio<sexo>(
              value: sexo.mujer,
              groupValue: _character,
              onChanged: (sexo? value) {
                setState(() {
                  _character = value;
                });
              },
            ),
          ),
          ListTile(
            title: const Text('Hombre'),
            leading: Radio<sexo>(
              value: sexo.hombre,
              groupValue: _character,
              onChanged: (sexo? value) {
                setState(() {
                  _character = value;
                });
              },
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.red, onPrimary: Colors.white),
            onPressed: () {print(_rutController);},
            child: Text('ENVIAR INFORMACION'),
          )
        ],
      ),
    );
  }
}

TextEditingController _rutController = TextEditingController();
TextEditingController _correoController = TextEditingController();
TextEditingController _nombreController = TextEditingController();

Container _rut() {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.black)),
    padding: const EdgeInsets.symmetric(horizontal: 15),
    margin: const EdgeInsets.symmetric(horizontal: 15),
    child: TextFormField(
      controller: _rutController,
      style: const TextStyle(fontSize: 15),
      decoration: const InputDecoration(
          hintText: "INGRESE SU RUT 12345678-0", border: InputBorder.none),
    ),
  );
}

Container _correo() {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.black)),
    padding: const EdgeInsets.symmetric(horizontal: 15),
    margin: const EdgeInsets.symmetric(horizontal: 15),
    child: TextFormField(
      controller: _correoController,
      style: const TextStyle(fontSize: 15),
      decoration: const InputDecoration(
          hintText: "INGRESE SU CORREO EJEMPLO@EJEM.COM",
          border: InputBorder.none),
    ),
  );
}

Container _nombre() {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.black)),
    padding: const EdgeInsets.symmetric(horizontal: 15),
    margin: const EdgeInsets.symmetric(horizontal: 15),
    child: TextFormField(
      controller: _nombreController,
      style: const TextStyle(fontSize: 15),
      decoration:
          const InputDecoration(hintText: "NOMBRES", border: InputBorder.none),
    ),
  );
}

Container _apellido() {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.black)),
    padding: const EdgeInsets.symmetric(horizontal: 15),
    margin: const EdgeInsets.symmetric(horizontal: 15),
    child: TextFormField(
      style: const TextStyle(fontSize: 15),
      decoration: const InputDecoration(
          hintText: "APELLIDOS", border: InputBorder.none),
    ),
  );
}

Container _direccion() {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.black)),
    padding: const EdgeInsets.symmetric(horizontal: 15),
    margin: const EdgeInsets.symmetric(horizontal: 15),
    child: TextFormField(
      style: const TextStyle(fontSize: 15),
      decoration: const InputDecoration(
          hintText: "DIRECCION", border: InputBorder.none),
    ),
  );
}
