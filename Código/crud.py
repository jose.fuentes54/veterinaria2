#Librerias
from tkinter import*
from tkinter import ttk
from tkinter import messagebox
from turtle import bgcolor
from webbrowser import BackgroundBrowser
from bson.objectid import ObjectId
import tkinter as tk
import pymongo

#Coneccion a MongoDB
myclient = pymongo.MongoClient("mongodb://localhost:27017")
mydb = myclient["Veterinaria"]
mycol = mydb["clientes"]
ID_CLIENTE=""

MONGO_HOST="localhost"
MONGO_PUERTO="27017"
MONGO_TIEMPO_FUERA=1000
MONGO_URI="mongodb://"+MONGO_HOST+":"+MONGO_PUERTO+"/"
MONGO_BASEDATOS="Veterinaria"
MONGO_COLECCION="clientes"
cliente=pymongo.MongoClient(MONGO_URI,serverSelectionTimeoutMS=MONGO_TIEMPO_FUERA)
baseDatos=cliente[MONGO_BASEDATOS]
coleccion=baseDatos[MONGO_COLECCION]

#Funciones
def mostrarDatos(rut="",mascota=""):
    objetoBuscar={}
    if len(rut)!=0:
        objetoBuscar["rut_cliente"]=rut
    if len(mascota)!=0:
        objetoBuscar["nombre_mascota"]=mascota
    try:
        registros=tabla.get_children()
        for registro in registros:
            tabla.delete(registro)
        id = 0
        for i in coleccion.find(objetoBuscar):
            tabla.insert(parent='',index='end',text=i["_id"],
            values=(i['rut_cliente'],i['nombre_cliente'],i['apellido_cliente'],i['telefono'],i['direccion'],i['nombre_mascota']))
            id +=1
        myclient.close()
    except pymongo.errors.ConnectionFailure as error:
        print(error)
        
def crearRegistro():
    if len(rut.get())!=0 and len(nombre.get())!=0 and len(apellido.get())!=0 and len(telefono.get())!=0 and len(direccion.get()) and len(mascota.get()) :
        try:
            myclient = pymongo.MongoClient("mongodb://localhost:27017")
            mydb = myclient["Veterinaria"]
            mycol = mydb["clientes"]
            mydict = {"rut_cliente":rut.get(),"nombre_cliente":nombre.get(),"apellido_cliente":apellido.get(),"telefono":telefono.get(),"direccion":direccion.get(),"nombre_mascota":mascota.get()}
            x = mycol.insert_one(mydict)
            print(x.inserted_id)
        except pymongo.errors.ConnectionFailure as error:
            print(error)
    else:
        messagebox.showerror(message="Los campos no pueden estar vacios")    
    mostrarDatos()

def dobleClickTabla(event):
    global ID_CLIENTE
    ID_CLIENTE=str(tabla.item(tabla.selection())["text"])
    documento=coleccion.find({"_id":ObjectId(ID_CLIENTE)})[0]
    rut.delete(0,END)
    rut.insert(0,documento["rut_cliente"])
    nombre.delete(0,END)
    nombre.insert(0,documento["nombre_cliente"])
    apellido.delete(0,END)
    apellido.insert(0,documento["apellido_cliente"])
    telefono.delete(0,END)
    telefono.insert(0,documento["telefono"])
    direccion.delete(0,END)
    direccion.insert(0,documento["direccion"])
    mascota.delete(0,END)
    mascota.insert(0,documento["nombre_mascota"])
    crear["state"]="disabled"
    editar["state"]="normal"
    borrar["state"]="normal"

def editarRegistro():
    global ID_CLIENTE
    try:
        idBuscar={"_id":ObjectId(ID_CLIENTE)}
        nuevosValores={"$set":{"rut_cliente":rut.get(),"nombre_cliente":nombre.get(),"apellido_cliente":apellido.get(),"telefono":telefono.get(),"direccion":direccion.get(),"nombre_mascota":mascota.get()}}
        coleccion.update_many(idBuscar,nuevosValores)
        rut.delete(0,END)
        nombre.delete(0,END)
        apellido.delete(0,END)
        telefono.delete(0,END)
        direccion.delete(0,END)
        mascota.delete(0,END)
    except pymongo.errors.ConnectionFailure as error:
        print(error)
    mostrarDatos()
    crear["state"]="normal"
    editar["state"]="disabled"
    borrar["state"]="disabled"

def borrarRegistro():
    global ID_CLIENTE
    try:
        idBuscar={"_id":ObjectId(ID_CLIENTE)}
        coleccion.delete_one(idBuscar)
        rut.delete(0,END)
        nombre.delete(0,END)
        apellido.delete(0,END)
        telefono.delete(0,END)
        direccion.delete(0,END)
        mascota.delete(0,END)
    except pymongo.errors.ConnectionFailure as error:
        print(error)
    crear["state"]="normal"
    editar["state"]="disabled"
    borrar["state"]="disabled"
    mostrarDatos()
    
def buscarRegistro():
    mostrarDatos(buscarCliente.get(),buscarMascota.get())

#Interfaz Usuario
ventana = Tk()
ventana.title("Veterinaria")

tabla = ttk.Treeview(ventana, columns =("#0","#2","#3","#4","#5","#6"))
tabla.grid(row = 1, column = 0, columnspan= 2)
tabla.heading("#0", text = "ID CLIENTE")
tabla.heading("#1", text = "RUT CLIENTE")
tabla.heading("#2", text = "NOMBRE CLIENTE")
tabla.heading("#3", text = "APELLIDO CLIENTE")
tabla.heading("#4", text = "TELEFONO")
tabla.heading("#5", text = "DIRECCION")
tabla.heading("#6", text = "NOMBRE MASCOTA")
tabla.bind("<Double-Button-1>",dobleClickTabla)

tablaScroll=ttk.Scrollbar(ventana,orient="vertical",command=tabla.yview)
tabla.configure(yscroll=tablaScroll.set)
tablaScroll.grid(row = 1,column=6,sticky="ns")

ventana.configure(bg='#f2e2e2')

style = ttk.Style()
style.theme_use("clam")

#RUT CLIENTE
Label(ventana,text="RUT CLIENTE",bg='#f2e2e2').grid(row = 2 , column = 0)
rut = Entry(ventana)
rut.grid(row = 2,column = 1, sticky = W + E)
rut.focus()

#Nombre Cliente
Label(ventana,text="NOMBRE CLIENTE",bg='#f2e2e2').grid(row = 3 , column = 0)
nombre = Entry(ventana)
nombre.grid(row = 3 ,column = 1, sticky = W + E)

#Apellido CLiente
Label(ventana,text="APELLIDO CLIENTE",bg='#f2e2e2').grid(row = 4 , column = 0)
apellido = Entry(ventana)
apellido.grid(row = 4 ,column = 1, sticky = W + E)

#Telefono
Label(ventana,text="TELEFONO",bg='#f2e2e2').grid(row = 5 , column = 0)
telefono = Entry(ventana)
telefono.grid(row = 5 ,column = 1, sticky = W + E)

#Direccion
Label(ventana,text="DIRECCION",bg='#f2e2e2').grid(row = 6 , column = 0)
direccion = Entry(ventana)
direccion.grid(row = 6 ,column = 1, sticky = W + E)

#Nombre Mascota
Label(ventana,text="NOMBRE MASCOTA",bg='#f2e2e2').grid(row = 7 , column = 0)
mascota = Entry(ventana)
mascota.grid(row = 7 ,column = 1, sticky = W + E)

#Boton Crear
crear = Button(ventana,text="CREAR CLIENTE", command = crearRegistro, bg = "#dfcae1", fg = "black")
crear.grid(row = 8,column = 1, sticky= W + E)

#Boton Editar
editar = Button(ventana,text="EDITAR CLIENTE", command = editarRegistro, bg = "#dfcae1", fg = "black")
editar.grid(row = 9,column = 1, sticky= W + E)
editar["state"]="disabled"

#Boton Eliminar
borrar = Button(ventana,text="BORRAR CLIENTE", command = borrarRegistro, bg = "#dfcae1", fg = "black")
borrar.grid(row = 10,column = 1, sticky= W + E)
editar["state"]="disabled"

#Boton Buscar
buscar = Button(ventana,text="BUSCAR CLIENTE", command = buscarRegistro, bg = "#dfcae1", fg = "black")
buscar.grid(row = 13,column = 1, sticky= W + E)

#Buscar Cliente
Label(ventana,text="BUSCAR POR RUT CLIENTE",bg='#f2e2e2').grid(row = 11,column = 0)
buscarCliente = Entry(ventana)
buscarCliente.grid(row = 11 , column = 1 , sticky = W + E)

#Buscar Mascosta
Label(ventana,text="BUSCAR POR NOMBRE DE MASCOTA",bg='#f2e2e2').grid(row = 12,column = 0)
buscarMascota = Entry(ventana)
buscarMascota.grid(row = 12 ,column = 1, sticky = W + E)

mostrarDatos()
ventana.mainloop()
